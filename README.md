# rabbitmq

## Prerequisites
Create a new directory.

Create a Dockerfile.dev.

Create a new Docker network.
docker network create --driver bridge rabbit-test

Start RabbitMQ in a docker container. We're going to give it the name "rabbitmq" so the producer and consumer can connect to it.
docker run -d  --name rabbitmq --hostname rabbitmq --network rabbit-test rabbitmq:3
That runs the RabbitMQ container in the background to enable typing in the terminal. It connects it to the rabbit-test network so that the other containers can talk to it.

Build an image with the Dockerfile.
docker build -f Dockerfile.dev . -t hello-rabbitmq

Run the image in a container.
docker run -it -v "$(pwd):/app" --network rabbit-test hello-rabbitmq

You should see the container prompt.

## Tutorial 1
Now we can try out our programs in a terminal. First, let's start a consumer, which will run continuously waiting for deliveries:

python receive.py
=> [*] Waiting for messages. To exit press CTRL+C
Now start the producer in a new terminal. The producer program will stop after every run:

python send.py
=> [x] Sent 'Hello World!'
The consumer will print the message:

=> [*] Waiting for messages. To exit press CTRL+C
=> [x] Received 'Hello World!'
Hurray! We were able to send our first message through RabbitMQ. As you might have noticed, the receive.py program doesn't exit. It will stay ready to receive further messages, and may be interrupted with Ctrl-C.

Try to run send.py again in a new terminal.

## Tutorial 2
First, let's try to run two worker.py scripts at the same time. They will both get messages from the queue, but how exactly? Let's see.

You need three consoles open. Two will run the worker.py script. These consoles will be our two consumers - C1 and C2.

shell 1
python worker.py
=> [*] Waiting for messages. To exit press CTRL+C
shell 2
python worker.py
=> [*] Waiting for messages. To exit press CTRL+C
In the third one we'll publish new tasks. Once you've started the consumers you can publish a few messages:

shell 3
python new_task.py First message.
python new_task.py Second message..
python new_task.py Third message...
python new_task.py Fourth message....
python new_task.py Fifth message.....
Let's see what is delivered to our workers:

shell 1
python worker.py
=> [*] Waiting for messages. To exit press CTRL+C
=> [x] Received 'First message.'
=> [x] Received 'Third message...'
=> [x] Received 'Fifth message.....'
shell 2
python worker.py
=> [*] Waiting for messages. To exit press CTRL+C
=> [x] Received 'Second message..'
=> [x] Received 'Fourth message....'
By default, RabbitMQ will send each message to the next consumer, in sequence. On average every consumer will get the same number of messages. This way of distributing messages is called round-robin. Try this out with three or more workers.

## Tutorial 3
We're done. If you want to save logs to a file, just open a console and type:

python receive_logs.py > logs_from_rabbit.log
If you wish to see the logs on your screen, spawn a new terminal and run:

python receive_logs.py
And of course, to emit logs type:

python emit_log.py
Using rabbitmqctl list_bindings you can verify that the code actually creates bindings and queues as we want. With two receive_logs.py programs running you should see something like:

sudo rabbitmqctl list_bindings
=> Listing bindings ...
=> logs    exchange        amq.gen-JzTY20BRgKO-HjmUJj0wLg  queue           []
=> logs    exchange        amq.gen-vso0PVvyiRIL2WoV3i48Yg  queue           []
=> ...done.

# Tutorial 4
If you want to save only 'warning' and 'error' (and not 'info') log messages to a file, just open a console and type:

python receive_logs_direct.py warning error > logs_from_rabbit.log
If you'd like to see all the log messages on your screen, open a new terminal and do:

python receive_logs_direct.py info warning error
=> [*] Waiting for logs. To exit press CTRL+C
And, for example, to emit an error log message just type:

python emit_log_direct.py error "Run. Run. Or it will explode."
=> [x] Sent 'error':'Run. Run. Or it will explode.'

## Tutorial 5
To receive all the logs run:

python receive_logs_topic.py "#"
To receive all logs from the facility "kern":

python receive_logs_topic.py "kern.*"
Or if you want to hear only about "critical" logs:

python receive_logs_topic.py "*.critical"
You can create multiple bindings:

python receive_logs_topic.py "kern.*" "*.critical"
And to emit a log with a routing key "kern.critical" type:

python emit_log_topic.py "kern.critical" "A critical kernel error"
Have fun playing with these programs. Note that the code doesn't make any assumption about the routing or binding keys, you may want to play with more than two routing key parameters.


## Tutorial 6
Our RPC service is now ready. We can start the server:

python rpc_server.py
=> [x] Awaiting RPC requests
To request a fibonacci number run the client:

python rpc_client.py
=> [x] Requesting fib(30)
The presented design is not the only possible implementation of an RPC service, but it has some important advantages:

If the RPC server is too slow, you can scale up by just running another one. Try running a second rpc_server.py in a new console.
On the client side, the RPC requires sending and receiving only one message. No synchronous calls like queue_declare are required. As a result the RPC client needs only one network round trip for a single RPC request.
