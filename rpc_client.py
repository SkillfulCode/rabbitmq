import pika
import uuid


class FibonacciRpcClient(object):

    def __init__(self):
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='rabbitmq'))

        self.channel = self.connection.channel()

        # In general doing RPC over RabbitMQ is easy. A client sends a request message and a server replies with a response message. In order to receive a response the client needs to send a 'callback' queue address with the request.
        result = self.channel.queue_declare(queue='', exclusive=True)

        # We establish a connection, channel and declare an exclusive callback_queue for replies.
        self.callback_queue = result.method.queue

        # We subscribe to the callback_queue, so that we can receive RPC responses.
        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True)

        self.response = None
        self.corr_id = None

    # The on_response callback that gets executed on every response is doing a very simple job, for every response message it checks if the correlation_id is the one we're looking for. If so, it saves the response in self.response and breaks the consuming loop.
    def on_response(self, ch, method, props, body):
        # In call method, we generate a unique correlation_id number and save it - the on_response callback function will use this value to catch the appropriate response.
        if self.corr_id == props.correlation_id:
            self.response = body

    # Next, we define our main call method - it does the actual RPC request.
    def call(self, n):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key='rpc_queue',
            properties=pika.BasicProperties(
                # reply_to: Commonly used to name a callback queue.
                reply_to=self.callback_queue,
                # Also in call method, we publish the request message, with two properties: reply_to and correlation_id.
                # correlation_id: Useful to correlate RPC responses with requests.
                correlation_id=self.corr_id,
            ),
            body=str(n))
        self.connection.process_data_events(time_limit=None)
        return int(self.response)


fibonacci_rpc = FibonacciRpcClient()

print(" [x] Requesting fib(30)")

# To illustrate how an RPC service could be used we're going to create a simple client class. It's going to expose a method named call which sends an RPC request and blocks until the answer is received:
# At the end we wait until the proper response arrives and return the response back to the user.
response = fibonacci_rpc.call(30)
print(" [.] Got %r" % response)
