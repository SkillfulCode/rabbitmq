import pika

# Our first program send.py will send a single message to the queue. The first thing we need to do is to establish a connection with RabbitMQ server.
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='rabbitmq'))
channel = connection.channel()
# We're connected now, to a broker on rabbitmq - hence the rabbitmq. If we wanted to connect to a broker on a different machine we'd simply specify its name or IP address here.

# Next, before sending we need to make sure the recipient queue exists. If we send a message to non-existing location, RabbitMQ will just drop the message. Let's create a hello queue to which the message will be delivered:
channel.queue_declare(queue='hello')

# At this point we're ready to send a message. Our first message will just contain a string Hello World! and we want to send it to our hello queue.
# In RabbitMQ a message can never be sent directly to the queue, it always needs to go through an exchange.
# We need to know now is how to use a default exchange identified by an empty string. This exchange is special ‒ it allows us to specify exactly to which queue the message should go. The queue name needs to be specified in the routing_key parameter:
channel.basic_publish(exchange='', routing_key='hello', body='Hello World!')
print(" [x] Sent 'Hello World!'")

# Before exiting the program we need to make sure the network buffers were flushed and our message was actually delivered to RabbitMQ. We can do it by gently closing the connection.
connection.close()
