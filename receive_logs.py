import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='rabbitmq'))
channel = connection.channel()

# create an exchange of that type, and call it logs.
# the fanout exchange broadcasts all the messages it receives to all the queues it knows.
channel.exchange_declare(exchange='logs', exchange_type='fanout')

#  we want to hear about all log messages, not just a subset of them. we're also interested only in currently flowing messages not in the old ones. to solve that we need two things:
# whenever we connect to rabbit we need a fresh, empty queue. to do it we could create a queue with a random name, or, even better - let the server choose a random queue name for us. we can do this by supplying empty queue parameter to queue_declare
# secondly, once the consumer connection is closed, the queue should be deleted. there's an exclusive flag for that:
result = channel.queue_declare(queue='', exclusive=True)
queue_name = result.method.queue

# now we need to tell the exchange to send messages to our queue. that relationship between exchange and a queue is called a binding. from now on the logs exchange will append messages to our queue.
channel.queue_bind(exchange='logs', queue=queue_name)

print(' [*] Waiting for logs. To exit press CTRL+C')


def callback(ch, method, properties, body):
    print(" [x] %r" % body)


channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()
